% !Tex root=extended-abstract.tex

\section{Introduction}
\label{sec:intro}
 
With the advent of digital controllers being increasingly used to control safety-critical cyber-physical systems, there is a growing need to provide formal correctness guarantees of such controllers. A recent approach to achieve this goal is the so-called \emph{Abstraction-Based Controller Design} (ABCD) \cite{tabuada2009verification,belta2017formal}.
In ABCD, a continuous dynamical system and a temporal specification are provided as the input, and a controller is automatically generated as the output, with the guarantee that the closed-loop (obtained by connecting the controller with the continuous dynamical system in feedback) satisfies the given specification.
ABCD is usually performed in three-steps.  
First, a finite state abstraction is computed from the continuous dynamical system by discretizing the state and input space.
Second, the given specification is lifted to the state space of the abstraction, and a discrete control strategy is synthesized using techniques from reactive synthesis. 
Finally, the discrete control strategy is refined to a continuous controller for the given system, which serves as the output of the procedure.
ABCD has been implemented in various tools for a variety of classes of systems and specifications, with numerous improvements over the basic procedure \cite{cosyma,rungger2016scots,mascot,pFaces}.

As common in most ABCD approaches, we study non-linear continuous dynamical systems with unknown, but bounded external disturbances. These systems are typically modelled by a differential inclusion
\begin{equation}\label{equ:F}
 \dot{x}\in F(x,u)+W,
\end{equation}
where $x$ is the state of the system, $\dot{x}$ is the first order time derivative of the state, $u$ is the input chosen by the (to be designed) controller, and $W$ is a bounded set of external disturbances.
Due to the existence of disturbances, a fixed controller results in an uncertain closed loop trajectory. 
The larger the set $W$ is, the larger is the uncertainty, which makes the controller synthesis task harder.

For systems modeled by \eqref{equ:F}, a controller synthesized using ABCD is provably correct if the actual disturbances experienced by the system during operation are a subset of the design disturbance set $W$. % considered during abstraction computation. %(Eq.~\eqref{equ:def_f}).
For this reason, ABCD techniques typically assume the most pessimistic disturbance bound when defining the set $W$.
This could be unreasonably pessimistic whenever the disturbances seen most of the time are small, but occasional high disturbance \enquote{spikes} may occur during operation. 
In this case, increasing $W$ to contain all possible disturbances may render the resulting abstract controller synthesis problem unrealizable, while a more optimistic choice of $W$ allows to design a controller but sacrifices rigorous correctness guarantees. That is, if a large disturbance spike occurs, the specification might be violated.

In our work, we address this trade-off between the increased pessimism and the reduced soundness guarantee of ABCD.
We automatically synthesize a controller which is correct w.r.t.\ a nominal disturbance $\Wnor$, and in addition, is \enquote{risk-minimizing} w.r.t.\ larger disturbance spikes in $\Whi\supset \Wnor$ that may occur any time. 
Intuitively, this leads to controllers that aim at fulfilling the given temporal specification robustly. As an illustrative example, consider the robot motion planning problem with a reach-avoid specification depicted in Fig.~\ref{fig:plots}. Here, the controller that we compute using our method takes the less risky wider passage through the blue obstacle even when the narrower passage would give a viable and the shortest path under the nominal disturbance.

Formally, we build on top of the basic three step procedure of ABCD. 
First, we obtain a special type of finite state abstraction for the given continuous dynamical system by adapting the framework of feedback-refinement relations \cite{ReissigWeberRungger_2017_FRR}; our abstraction has ``normal edges'' and ``disturbance edges'' to capture operation with disturbance $\Wnor$ and $\Whi\setminus \Wnor$, respectively.
Second, we compute a maximally-resilient control strategy for this abstraction by an adaptation of the two-player game algorithm to compute \emph{Optimally Resilient Strategies} as defined by \citeauthor{DBLP:conf/csl/NeiderW018}~\cite{dallal2016synthesis,DBLP:conf/csl/NeiderW018}. 
In particular, we first solve a game for the given specification over the graph induced by the normal edges of the abstraction.
This results in a control strategy for the abstract system which is correct-by-construction for the $\Wnor$ disturbance set. 
To take the high disturbance set $\Whi$ into account, we then compute the optimally resilient control strategy on top of the nominal solution. This is achieved by solving a series of games over the complete game graph by taking the disturbance edges into account.
This synthesis algorithm is modular in the sense that it can handle arbitrary $\omega$-regular winning conditions when provided with a solver for the disturbance-free game (under some additional mild assumption).
Finally, the control strategy so obtained can be refined to a continuous controller for the original system, while retaining the correctness guarantees and the resilience values.

This three step procedure is what we call the \emph{Resilient Abstraction-Based Controller Design}.


