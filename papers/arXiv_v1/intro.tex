% !Tex root=main.tex

\section{Introduction}
\label{sec:intro}

With the advent of digital controllers being increasingly used to control safety-critical cyber-physical systems (CPS), there is a growing need to provide formal correctness guarantees of such controllers. A recent approach to achieve this goal is the so-called \emph{Abstraction-Based Controller Design} (ABCD) \cite{belta2017formal,tabuada2009verification}.
ABCD is usually performed in three-steps.  
First, a finite state abstraction is computed from a given non-linear continuous dynamical system by discretizing the state and input space.
Second, given this abstraction and a linear-time temporal logic specification, a discrete control strategy is synthesized. 
Finally, the discrete control strategy is refined to a continuous controller for the given system, which serves as the output of the procedure.
ABCD has been implemented in various tools for a variety of classes of systems and specifications, with numerous improvements over the basic procedure (e.g.,~ \cite{cosyma,rungger2016scots,mascot,pFaces,ROCS}).

An important feature of controllers for CPS is their robustness against modelling uncertainties and unforeseen operating conditions. The ABCD workflow therefore has a build-in robutstification step; given a uniform upper bound $W$ on the uncertainty of continuous trajectories, the abstract transition system over-approximates all possible trajectories of the original system. 

This approach, however, has one important disadvantage. Consider for example a mobile robot serving coffee in an office building. As it needs to get to the kitchen to get coffee, it sometimes passes through water spilled on the floor which results in an imprecise motion actuation.

In this case, increasing $W$ to contain all possible disturbances caused by spilled water would result in a very conservative abstract model which
renders the resulting abstract controller synthesis problem (e.g., always eventually serving a requested coffee) unrealizable. A more optimistic choice of $W$ allows to design a controller but sacrifices rigorous correctness guarantees --  if water is spilled, the specification might be violated (e.g., the robot might bump into a door frame). Intuitively we however know that it is very unlikely that water is spilled \emph{everywhere} and \emph{all the time}, which would indeed make the specification unrealizable. Further, knowing that there might be spilled water, we would like the robot motion controller to be \enquote{risk-aware} (i.e., to avoid going close to the door frames). 

\smallskip
\noindent\textbf{Contribution.}
In our work, we automatically synthesize a controller which is correct w.r.t.\ a nominal disturbance $\Wnor$ (e.g., no water spilled on the floor), and in addition, is \enquote{risk-minimizing} w.r.t.\ larger disturbance spikes in $\Whi\supset \Wnor$ that may occur any time. 

Formally, we build on top of the basic three step procedure of ABCD. 
First, we obtain a \emph{risk-aware} finite state abstraction for the given continuous dynamical system by adapting the framework of feedback-refinement relations \cite{ReissigWeberRungger_2017_FRR}.
Second, we compute a maximally-resilient control strategy for this abstraction by an adaptation of the two-player game algorithm to compute \emph{Optimally Resilient Strategies} as defined by Neider et al.~\cite{dallal2016synthesis,DBLP:journals/acta/NeiderWZ20}. 
Finally, the control strategy so obtained can be refined to a continuous controller for the original system, while retaining the correctness guarantees and the resilience of the states.
This three step procedure is what we call the \emph{Resilient Abstraction-Based Controller Design}.

\smallskip
\noindent\textbf{Related Work.}
Ensuring robustness of discrete, \emph{event-based} control strategies is an active field of research.  
Within \emph{resilient ABCD}, we employ the method introduced in \cite{dallal2016synthesis,DBLP:journals/acta/NeiderWZ20}, 
where \emph{disturbance edges} are utilized to formalize resilience and are assumed to be given. Unfortunately, a good disturbance model is not always easy to obtain. 
Our \emph{resilient ABCD} method \emph{automatically} injects disturbance edges into the abstract model. 

Another approach to robust reactive synthesis considers 
particular specifications of the form $A\rightarrow G$; in every environment that fulfills the assumption $A$ the controller needs to enforce the property $G$. In this context robustness is for example understood w.r.t.\ assumption violations \cite{bloem2014handle,EhlersTopcu_14, Hagihara_16}, 
hidden or missing inputs \cite{ Bloem_19} or unexpected jumps in the game graph
\cite{DLMurray_ICCPS17}. 
While the abstraction generated within our \emph{resilient ABCD} method can be understood as a particular safety assumption $A$ for the given synthesis problem interpreted as $G$, the assumption violations we consider are already explicitly modeled by \emph{automatically generated} disturbance edges. This avoids analyzing assumptions for possible faults and allows for a more fine-grained analysis assigning resilience values to states rather than whole systems.
While lifting the synthesis of robust controllers to the abstract domain, \emph{resilient ABCD} still retains the intuitive local nature of \emph{robustness} of continuous systems by injecting disturbance edges only locally. This is closely related to work in robust CPS design, where continuous and discrete metrics are imposed to construct abstractions for robust controller synthesis \cite{RunggerTabuada_TAC16, tabuada2014towards,LiuOzay_16}. In this context, robustness ensures that the controlled system remains \enquote{close} to a chosen execution path despite disturbances.
In some cases, a similar sort of robustness is achieved with the help of online monitoring of the system and updating the controller if unexpected events occur \cite{wongpiromsarn2012receding}.
In contrast, \emph{resilient ABCD} exploits the existence of multiple different control strategies and picks the most resilient among them. This optimizes resilience over the infinite time horizon of the system's execution.

An alternate approach to ours would be to assign probability values to disturbances: The ones in $\Whi\setminus \Wnor$ occur with low probability, and the ones in $\Wnor$ occur with high probability.
Then an optimal controller that maximizes the probability of satisfaction of the given specification \cite{dutreix2020abstraction, majumdar2020symbolic, svorevnova2017temporal} would perhaps behave similarly to the optimally resilient controller in our setup.
In contrast to our approach, there are two drawbacks of the probabilistic treatment of unexpected disturbances: 
First, an explicit probabilistic model of the disturbance is required, which is often difficult to obtain.
Second, just by knowing the optimal satisfaction probability of the specification from a given state, one cannot tell whether an absence of larger disturbance would surely ensure the satisfaction of the specification.

For particular classes of continuous-time systems and temporal logic specifications, controllers can be designed without explicitly constructing an abstraction by state-space gridding. In this line of work, robustness requirements can be specified by  signal temporal logic (STL) formulas and enforced through \enquote{classical} optimal robust controller synthesis methods \cite{sadraddini2015robust,lindemann2019robust,mehdipour2019arithmetic}.
 
Our \emph{resilient ABCD} method is orthogonal to this line of work, as we \emph{lift} the treatment of robustness to the abstract domain. This allows us to handle more general specification classes, namely full linear temporal logic (LTL), and arbitrary non-linear continuous dynamics. 
Further, \emph{resilient ABCD} allows to incorporate other discrete disturbances s.a.\ lossy channels or faulty event models \cite{girault2009automating,YuJiang_review_15}.