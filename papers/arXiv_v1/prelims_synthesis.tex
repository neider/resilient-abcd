\subsection{Preliminaries}

Let $\Prob=(\SysT,\Whi,\Spec)$ be a given risk-aware controller synthesis problem, $\Gamma=(\Xa,\Ua,\fanor,\fadist)$ a risk-aware abstraction of $\SysT$, and $R$ the associated FRR between $\SysT$ and $(\Xa,\Ua,\fanor)$.
We assume that the parity specification $\Spec$ is so given and $\Gamma$ is so obtained that for every abstract state $q\in \Xa$, every pair of associated system states $x_1,x_2 \in \Xc$ with $(x_1,q),(x_2,q)\in R$ are assigned the same color by $\Spec$ (i.e.,\ $\Spec(x_1)=\Spec(x_2)$).
This allows us to lift the parity specification $\mathit{Parity}(\Spec)$ in a well-defined way to the abstract state space as $\mathit{Pairty}(\widehat{\Spec})\subseteq \Xa^\omega$, s.t.\ for all $q\in \Xa$ and for all $x\in \Xc$ with $(x,q)\in R$, $\widehat{\Spec}:q\mapsto \Spec(x)$.
We denote the \emph{abstract risk-aware controller synthesis problem}, or abstract synthesis problem in short, by $\ProbA=(\Gamma,\widehat{\Spec})$.

We export the concepts of controllers, closed-loops, closed-loop trajectories etc.\ from the domain of sampled-time control systems to the domain of risk-aware abstractions in the natural way.
An abstract controller $\widehat{C}$ is a partial function $\widehat{C}:\Xa\rightarrow \Ua$.
The abstract closed-loop is obtained by connecting $\widehat{C}$ in feedback with $\Gamma$, and is defined using the tuple $\Gamma\parallel \widehat{C}=(\dom{\widehat{C}}, \faC)$, where $\faC:\dom{\widehat{C}}\setmap \Xa$ s.t.\ $\faC:q\mapsto \fanor(q,\widehat{C}(q))\cup \fadist(q,\widehat{C}(q))$.
The notion of trajectory, the satisfaction of specification, $\NSpikes$, spike-free trajectories, and resilience are naturally adapted for the system $\Gamma$.
It should be noted, however, that $\NSpikes$ and resilience are now computed by \emph{counting the number of $\fadist$-transitions} appearing in any given abstract closed-loop trace, as opposed to counting the number of disturbances appearing from the set $\Whi\setminus\Wnor$ in a given sampled-time closed-loop trace.

Like in the case of controller synthesis problem $\Prob$, we require our abstract controllers to be sound and maximal.
Such a controller is called a \emph{winning controller} w.r.t.\ a given abstraction specification $\mathit{Win}\subseteq \Xa^\omega$, and the respective controller domain is called the winning region $\mathcal W(\ProbA)$.
Let $\mathcal{C}^{\Gamma,\mathit{Win}}$ be the set of winning abstract controllers for the abstract synthesis problem $\ProbA=(\Gamma,\mathit{Win})$.
We assume that we have access to a solver for the sound and maximal abstract controllers for the (spike-free) safety and parity specifications, which serves as a black-box method in our synthesis routine (for the actual implementation, one can use any of the available methods from the literature \cite{van2018oink}).
Such a solver takes a controller synthesis problem $\ProbA$ with safety, parity, or a conjunction of safety and parity specification as input, and outputs the winning region $\mathcal W(\ProbA)$ (as well as the complement $\overline{\mathcal W}(\ProbA)$) together with a (uniform) abstract controller $\widehat{C}$ that is winning for every abstract state $\xa \in \mathcal W(\ProbA)$.

%---------- Resilience and Optimally Resilient Abstract Controllers ----------

We define the resilience of abstract states $r_{\ProbA}(\xa)$ and the optimally resilient abstract controller $\widehat{C}^*$ analogously to the continuous system defined in \REFsec{sec:problem}.
Recall that a $k$-resilient control strategy with $k \in \omega$ is winning even under at most $k-1$ disturbance spikes, an $\omega$-resilient strategy is winning even under any finite number of disturbance spikes, and an $(\omega + 1)$-resilient strategy is winning even under infinitely many disturbance spikes.