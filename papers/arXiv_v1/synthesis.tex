% !Tex root=main.tex

\section{Synthesis of Optimally Resilient Controllers}\label{sec:synth}

%---------- Daniel ----------
In the following, we present our algorithm to synthesize the optimally resilient controller for risk-aware abstractions of sampled-time control systems.
Our algorithm follows the general ideas of \cite{DBLP:journals/acta/NeiderWZ20}, and assigns resilience values to every abstract state of the risk-aware abstraction.
Intuitively, the resilience of an abstract state corresponds to the maximum number of disturbance spikes that the \emph{abstract closed-loop}---formed by connecting the synthesized controller with the abstract system in feedback---can tolerate while still satisfying its specification.
We are interested to synthesize controllers which \emph{maximize} the resilience of each abstract state.
Before we describe our algorithm we introduce required definitions and notation.

%---------- Basic Definitions ----------
\input{prelims_synthesis}

%---------- Computing Optimally Resilient Strategies ----------
\subsection{Computing Optimally Resilient Strategies}
Following Neider, Weinert, and Zimmermann~\cite{DBLP:journals/acta/NeiderWZ20}, we first characterize the abstract states of finite resilience.
The remaining abstract states then have either resilience $\omega$ or $\omega +1$, and we show how to distinguish between them.
Finally, we describe how to derive an optimally resilient abstract controller based on the computed resilient values.

\subsubsection{Finite Resilience}
Starting from the abstract states in $\overline{\mathcal W}(\ProbA)$, which have resilience $0$ by definition, we use two operations to determine the abstract states of finite resilience: the disturbance update and the risk update.
Intuitively, the disturbance update computes the resilience of abstract states for which a disturbance spike (i.e., a transition in $\fadist$) leads to an abstract state whose resilience is already known.
The risk update, on the other hand, determines the resilience of abstract states from which the controller can either not prevent to visit an abstract state with known resilience or it needs to move to such an abstract state in order to avoid losing.

For the remainder, let us fix an abstract synthesis problem $\ProbA = (\Gamma, \widehat{\Phi})$ with risk-aware abstraction $\Gamma=(\Xa,\Ua,\fanor,\fadist)$.
Following Neider, Weinert, and Zimmermann~\cite{DBLP:journals/acta/NeiderWZ20}, we define the disturbance and risk updates as updates on partial mappings $r \colon \Xa \to \omega$, which are called \emph{rankings}.
Intuitively, a ranking assigns resilience to some of the abstract states.
We denote the domain of $r$ by $\mathrm{dom}(r)$ and the image of $r$ by $\mathrm{im}(r)$.

Due to the different problem setup, we now deviate from Neider, Weinert, and Zimmermann's original algorithm in that we perform disturbance and risk updates not on the same risk-aware abstraction but on a sequence $(\Gamma_i)_{i=1, 2, \ldots}$ of abstractions.
We obtain $\Gamma_{i+1}$ from $\Gamma_i$ using an operation we call \emph{strategy pruning}, which removes certain transitions from $\Gamma_i$ that are no longer relevant for the computation of resilience values.

Strategy pruning is inter-weaved with the disturbance and risk updates as shown in Algorithm~\ref{alg:computing-finite-resilience}.
Our algorithm starts with the \emph{initial ranking} that assign the value $0$ to all $\xa \in \overline{\mathcal W}(\Gamma)$ and is otherwise undefined.
Then, it computes the disturbance update on $\Gamma_i$, applies strategy pruning to obtain $\Gamma_{i+1}$, and finally computes the risk update on $\Gamma_{i+1}$.
This process repeats until a fixed point is reached (i.e., $r_i = r_{i-1}$), at which point the algorithm returns the final ranking $r^\ast = r_i$.
The ranking $r^\ast$ then maps an abstract state to its resilience.

\begin{algorithm}
	\begin{algorithmic}[1]
		\INPUT Risk-aware abstraction $\Gamma$
		
		\State Compute $\overline{\mathcal W}(G)$
		\State Initialize an initial ranking $r_0$ with $r_0(\xa) = 0$ for all $\xa \in \overline{\mathcal W}(G)$ and \emph{undefined} otherwise
		\State $i \gets 0$ and $\Gamma_0 \gets \Gamma$
		
		\Repeat
			\State $r' \gets \mathit{disturbance\textunderscore{}upd}(r_i, \Gamma_i)$
			\State $\Gamma_{i+1} \gets \mathit{strategy\textunderscore{}pruning}(r_i,\Gamma_i)$
			\State $r_{i+1} \gets \mathit{risk\textunderscore{}upd}(r', \Gamma_{i+1})$
			\State $i \gets i + 1$
		\Until{$r_i = r_{i-1}$}

		\State \Return $r^\ast = r_i$

	\end{algorithmic}
	\caption{Determining finite resilience} \label{alg:computing-finite-resilience}
\end{algorithm}

In the remainder of this section, we describe all three operations in detail.

%----------- Disturbance update ----------
\paragraph{Disturbance Update}
As mentioned before, the intuition behind the disturbance update, which we denote by $\mathit{disturbance\textunderscore{}upd}$, is to compute the resilience of abstract states for which a disturbance spike leads to an abstract state whose resilience is already known.
It takes two inputs: a ranking $r$ and a risk-aware abstraction $\Gamma = (\Xa, \Ua, \fanor, \fadist)$.

In the first step, the disturbance update computes for each $\xa \in \Xa$ a set $S_\xa\subseteq \Xa$ which satisfies the condition given below: 
\begin{align*}
	\xa'\in S_\xa \Leftrightarrow {} & [\text{for all $\ua \in \Ua$,} \\
	& \fanor(\xa, \ua) \neq \emptyset \text{ and } \fanor(\xa, \ua) \cap \mathrm{dom}(r) = \emptyset \\
	& \text{imply } \fadist(\xa, \ua) \cap \mathrm{dom}(r) \neq \emptyset] \\
	& \text{and } \\
	& [\text{there exists $\ua \in \Ua$,} \\
	& \fanor(\xa, \ua) \neq \emptyset \text{ and } \fanor(\xa, \ua) \cap \mathrm{dom}(r) = \emptyset \\
	& \text{and } \xa' \in \fadist(\xa, \ua) \text{ and } \xa' \in \mathrm{dom}(r)].
\end{align*} 
Then, it returns a new ranking $r'$ with
\[ r'(\xa) = \min \bigl\{ \{ r(\xa) \} \cup \{ r(\xa') + 1 \mid \xa' \in S_\xa \} \bigr \} \]
for each $\xa \in \Xa$, where $\{ r(\xa) \} = \emptyset$ if $\xa \notin \mathrm{dom}(r)$, and $\min{\emptyset}$ is undefined. 

%----------- Strategy Pruning ----------
\paragraph{Strategy Pruning}
The strategy pruning step, which we denote by $\mathit{strategy\textunderscore{}pruning}$, removes transitions from a risk-aware abstraction that are no longer relevant for the computation of resilience values.
It takes two inputs: a ranking $r$ and a  risk-aware abstraction $\Gamma = (\Xa, \Ua, \fanor, \fadist)$.

Based on the transition function $\fanor$ and $\fadist$, strategy pruning first computes two new transition functions ${\fanor}'$ and ${\fadist}'$ where
\begin{align*}
	{\fanor}'(\xa, \ua)  & = \begin{cases} \emptyset & \text{if $F$ is true; and} \\ \fanor(\xa, \ua)  & \text{otherwise} \end{cases}
	%
	\intertext{for all $\xa \in \Xa$ and $\ua \in \Ua$ as well as}
	%
	{\fadist}'(\xa, \ua)  & = \begin{cases} \emptyset & \text{if $F$ is true; and} \\ \fadist(\xa, \ua)  & \text{otherwise} \end{cases}
\end{align*}
for all $\xa \in \Xa$ and $\ua \in \Ua$, and where $F$ is true if and only if $\fanor(\xa, \ua) \cap \mathrm{dom}(r) \neq \emptyset$ or $\fadist(\xa, \ua) \cap \mathrm{dom}(r) \neq \emptyset$.
Then, it returns the new risk-aware abstraction $\Gamma' = (\Xa, \Ua, {\fanor}', {\fadist}')$.


%----------- Risk Update ----------
\paragraph{Risk Update}
The risk update, which we denote by $\mathit{risk\textunderscore{}upd}$, determines the resilience of abstract states from which the controller can either not prevent to visit an abstract state with known resilience or it needs to move to such an abstract state in order to avoid losing.
Like the disturbance update, it takes two inputs: a ranking $r$ and a risk-aware abstraction $\Gamma = (\Xa, \Ua, \fanor, \fadist)$.

For each $k \in \mathrm{im}(r)$, the risk update computes the set
\begin{multline*}
	B_k = \overline{\mathcal W} \bigl(\Gamma, \mathit{Parity}(\Phi) \cap {} \\
	\mathit{Safety}( \{x \in \mathrm{dom}(r) \mid r(x) \leq k \}) \bigr).
\end{multline*}
As described above, this can be done by first solving the safety game and then the parity game on the resulting game.
As a byproduct, we obtain in iteration $i$ a controller that (i) never visits a state with resilience less than $i$ and (ii) is winning if no disturbance spike occurs.
Once the fixed point is reached, the final controller can even tolerate an arbitrary number of resilience spikes.

Then, the risk update returns the new ranking $r'$ with
\[ r'(\xa) = \min{\{ k \mid \xa \in B_k \}}, \]
for every $\xa \in \Xa$, where again $\min{\emptyset}$ is undefined.
This concludes the computation of finite resiliences.


%---------- Distinguishing Between Resilience $\omega$ and $\omega+1$ ----------
\subsubsection{Distinguishing Between Resilience $\omega$ and $\omega+1$}
It is left to distinguish between abstract states of resilience $\omega$ and $\omega + 1$.
Recall that abstract states of resilience $\omega + 1$ are those from which the controller can satisfy the specification even if infinitely many disturbance spikes occur.
Fortunately, characterizing the abstract states of resilience $\omega + 1$ is straightforward.

We solve the classical controller synthesis problem for the abstraction $(\Xa, \Ua, \fanor\cup \fadist)$ (i.e., the abstraction that always allows for large disturbances), where $\fanor\cup\fadist$ denotes the argument-wise union operation of the set-valued transition functions $\fanor$ and $\fadist$.
In fact, it is  then not hard to verify that the abstract states in $\mathrm{dom}(\widehat{C})$ of the resulting abstract controller $\widehat{C}$ are exactly those of resilience $\omega + 1$, and the controller is $(\omega + 1)$-resilient from these abstract states.
This is due to the fact that $\widehat{C}$ can enforce the specification even under arbitrary many occurrences of high disturbances.

In total, we have identified the abstract states of finite resilience and resilience $\omega + 1$ of a risk-aware abstraction $\Gamma = (\Xa, \Ua, \fanor, \fadist)$.
The remaining abstract states in $\Xa$ must then have resilience $\omega$.
This concludes the computation of the function $r^\ast$, which maps an abstract state to its resilience, and we can  now describe how to extract an optimally resilient controller. 


%---------- Extracting Omptimally Resilient Strategies ----------
\subsubsection{Extracting Optimally Resilient Strategies}
The extraction of an optimally resilient abstract controller follows very closely the one by Neider, Weinert, and Zimmermann~\cite{DBLP:journals/acta/NeiderWZ20}. 
Intuitively, the controller extraction is the process of stitching together the controllers that were obtained in different iterations of the risk update and the $(\omega+1)$-resilience computation.
The underlying idea is to switch the controller whenever a disturbance spike occurs.
If finitely many disturbance spikes occur, then the optimally resilient controller will settle with an appropriate \mbox{(sub-)}controller that was obtained in the risk update.
If infinitely many disturbance spikes occur, then the optimally resilient controller will settle with the \mbox{(sub-)}controller obtained in the $(\omega+1)$-resilience computation.
In both cases, these (sub-)controllers are winning by construction and, hence, so is the optimally resilient one.

% ------- Informal treatment of controller refinement ------------
\subsubsection{Controller refinement}
Let $\Prob=(\SysT,\Whi,\Spec)$ be a risk-aware controller synthesis problem.
Let  $\widehat{C}^*$ be  an optimally resilient controller synthesized for the abstract synthesis problem $\widehat{\Prob}=(\Gamma,\widehat{\Spec})$, where $\Gamma$ is a risk-aware abstraction of $\SysT$ with the corresponding FRR $R$.
Following the usual methodology of ABCD, one can define a controller for $\SysT$ as $C:\xc\mapsto \widehat{C}^*(\xa)$ where $\xa\in \Xa$ s.t.\ $(\xc,\xa)\in R$. Here, $C$ is well-defined due to the properties of $R$.
It can be shown that for every $\xc\in \Xc$, $C$ is an $\alpha$-resilient controller for some $\alpha \leq \risk(\xc)$ (i.e.,\ $C$ is a sub-optimal approximation of the optimally resilient controller $C^*$).