\subsection{Preliminaries}\label{sec:prelims_abcs}

The method that we primarily build upon is Abstraction-Based Control Design (ABCD).
In the following, we briefly recapitulate one of the several available ABCD techniques.

\smallskip
\noindent\textbf{Finite State Transition System.}
A \emph{finite state transition system} is a tuple $\Delta= (\Xa, \Ua, \fa)$ that consists of a finite set of states $\Xa$, a finite set of control actions $\Ua$, and a set-valued transition map $\fa:\Xa\times \Ua\setmap \Xa$.

\smallskip
\noindent\textbf{Finite State Abstraction of Control Systems.}
A finite state transition system $\Delta$ is called a \emph{finite state abstraction}, or simply an abstraction, of a given control system $\Sys$ if a certain relation holds between the transitions of $\SysT$ and the transitions of $\Delta$.
Depending on the controller synthesis problem at hand, there are several such relations available in the literature.
The one that we use in our work is the \emph{feedback refinement relation} (FRR) \cite{ReissigWeberRungger_2017_FRR}.

An FRR is a relation $R\subseteq \Xc\times \Xa$ between $\SysT$ and a finite-state transition system $\Delta$, written as $\SysT \preccurlyeq_R \Delta$, so that for every $(\xc,\xa)\in R$, the set of allowed control inputs (element of $\Ua$) from $\xa$ is a subset of the set of allowed control inputs (element of $\Uc$) from $\xc$, and moreover when the same allowed control input is applied to both $\xa$ and $\xc$, the image of the set of possible successors of $\xc$ under $R$ is contained in the set of successors of $\xa$.

Within this paper we assume that there is an algorithm, called $\proc$, which takes as input a given control system $\Sys$ and a given set of additional tuning parameters $P$ (like the state space discretization and the control space discretization), and outputs an abstract finite state transition system $\Delta$ and an associated FRR $R$ s.t.\ $\SysT\preccurlyeq_R \Delta$.
For the actual implementation of $\proc(\Sys,P)$, we refer the reader to \cite{ReissigWeberRungger_2017_FRR}.

Without going into the details of how the parameter set $P$ influences the outcome of $\proc$, we would like to point out one property that we expect to hold.
Let $\SysT=(\Xc,\Uc,\Wnor,\fc)$ and $\SysT'=(\Xc,\Uc,\Wnor',\fc')$ be two different control systems with same state and control input spaces.
Suppose $\Abs=(\Xa,\Ua,\fa)$ and $\Abs'=(\Xa',\Ua',\fa')$ be two transition systems computed using $\proc$ using the same parameter set $P$ (i.e.,\ $\proc(\SysT,P)=\Abs$ and $\proc(\SysT',P)=\Abs'$).
Then there are one-to-one correspondences between $\Xa$ and $\Xa'$, and between $\Ua$ and $\Ua'$.
Moreover, there is an FRR $R$ so that both $\SysT\preccurlyeq_R \Abs$ and $\SysT'\preccurlyeq_R \Abs'$ hold.
We will abuse this property, and will use the same state space and input space for both $\Abs$ and $\Abs'$.