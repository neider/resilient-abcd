\section{Preliminaries}

\KM{This section is copied almost verbatim from the CDC2018 paper on Lazy Abstraction-Based Control for Safety Specification.}

\noindent\textbf{Notation.}
%
Let $\mathbb{R}$, $\mathbb{N}$, and $\mathbb{N}_0$ be respectively the set of real numbers, the set of natural numbers, and the set $\mathbb{N}\cup \set{0}$ respectively.
Given $a,b\in\real{}\cup\set{\pm\infty}$ with $a < b$, we denote by $[a,b]$ a closed interval. 
Given $a,b\in(\real{}\cup\set{\pm\infty})^n$, we denote by $a_{i}$ and $b_{i}$ their $i$-th element. 
A \emph{cell} $\hyint{a,b}$ with $a<b$ (component-wise) is the closed set $\set{x\in\real{}^n\mid a_i\leq x_i\leq b_i}$.
We define the relations $<,\leq,\geq,>$ on $a,b$ component-wise. 
% 
For a set $A$, we write $A^*$ and $A^\infty$ for the set of finite, and the set of finite or infinite sequences over $A$, respectively.
For $w\in A^*$, we write $|w|$ for the length of $w$; the length of an infinite sequence is $\infty$. 
For $0 \leq k < |w|$ we write $w(k)$ for the $k$-th symbol of $w$.
 

\smallskip
\noindent\textbf{Continuous-Time Control System.}\
A \emph{control system} $\Sigma = (X, U, W, f)$
consists of a state space $X= \real{n}$, a non-empty input space $U\subseteq\real{m}$, 
a compact set $W\subset \real{n}$,
and a function $f:X\times U \rightarrow \real{n}$ locally Lipschitz in the first argument s.t.
\begin{equation}\label{equ:def_f}
 \dot{\xi}\in f(\xi(t),u(t))+W
\end{equation}
holds.  
% 
Given an initial state $\xi(0)\in X$, a positive parameter $\tau>0$ and a constant input trajectory $\mu_u:[0,\tau]\rightarrow U$
which maps every $t\in [0,\tau]$ to the same $u\in U$, 
a solution of the inclusion in \eqref{equ:def_f} 
on $[0,\tau]$ is an absolutely continuous function $\xi:[0,\tau]\rightarrow X$  
that fulfills \eqref{equ:def_f} for almost every $t\in[0,\tau]$. 
We collect all such solutions in the set $\ON{Sol}_f(\xi(0),\tau,u)$. 

\smallskip
\noindent\textbf{Time-Sampled System.}\
Given a time sampling parameter $\tau>0$, we define the \emph{time-sampled system} $\St(\Sigma,\tau)=(X,U,\Ft)$ associated with $\Sigma$, 
where $\Ft:X\times U\fun 2^X$ is the transition function, defined s.t.\ for 
all $x\in X$ and for all $u \in U$ it holds that $x'\in \Ft(x,u)$ iff there exists a solution $\xi\in\ON{Sol}_f(x,\tau,u)$ 
s.t.\ $\xi(\tau)=x'$.
A \emph{trajectory} $\xit$ of $\St(\Sigma,\tau)$ is a finite or infinite sequence $x_0\xrightarrow{u_0}x_1\xrightarrow{u_1} \ldots$
such that for each $i\geq 0$, $x_{i+1}\in\Ft(x_i, u_i)$; the collection of all such trajectories defines the behavior $\Beh{\St(\Sigma,\tau)}\subseteq X^\infty$.

 
\smallskip
\noindent\textbf{Abstract System.}\ 
A \emph{cover} $\hat{X}$ of  $X$ is a set of non-empty cells $\hyint{a,b}$ with $a,b\in (\real{}\cup\Set{\pm\infty})^n$,
s.t.\ every $x\in X$ belongs to some cell $\xa\in\hat{X}$. 
We fix a grid parameter $\eta \in\real{}_{>0}^n$ and a global safety requirement
$Y = \hyint{\alpha, \beta}$, s.t.\ $\beta - \alpha$ is an integer multiple of $\eta$.
% Then we can define a \emph{finite} cover of $Y$ using cells whose size is $\eta$.
A point $c\in Y$ is \emph{grid-aligned} if there is $k\in\Z^n$ s.t. for each $i\in [1;n]$,
$c_i = \alpha_i + k_i\eta_i - \frac{\eta_i}{2}$.
A cell $\hyint{a,b}$ is \emph{grid-aligned} if there is a grid-aligned point $c$ s.t.\ $a = c - \frac{\eta}{2}$ and
$b = c + \frac{\eta}{2}$;
such cells define sets of diameter $\eta$ whose center-points are grid aligned. 
Clearly, the set of grid-aligned cells is a \emph{finite cover} for $Y$.

We define an \emph{abstract system} $\Sa(\Sigma,\tau,\eta)=(\Xa,\Ua,\Fa)$ s.t.\ the following holds:
% 
\begin{inparaenum}[(i)]
 \item $\Xa$ is a finite cover of $X$ and there exists a non-empty subset 
 $\Ya\subseteq\Xa$ which is a cover of $Y$ with grid aligned cells,
 \item $\Ua\subseteq U$ is finite,
 \item $\Fa:\Xa\times \Ua\rightarrow 2^{\Xa}$ is the transition function s.t. for all $\xa\in(\Xa\setminus\Ya)$ and $u\in\Ua$ it holds that $\Fa(\xa,u)=\emptyset$, and
 \item for all $\xa\in\Ya$, $\xa'\in\Xa$, and $u\in\Ua$ it holds that\footnote{We use the technique explained in \cite{ReissigWeberRungger_2017_FRR} and implemented in 
\texttt{SCOTS} \cite{Scots} to over-approximate the set $\set{\cup_{x\in\xa}\ON{Sol}_f(x,\tau,\ua)}$ in \eqref{eq:next state abs sys 0}.} 
 \begin{align}\label{eq:next state abs sys 0}
   \propAequ{\xa'\in\Fa(\xa,u)}{\set{\cup_{x\in\xa}\ON{Sol}_f(x,\tau,\ua)} \cap \xa' \neq \emptyset}.
 \end{align}
\end{inparaenum}
% 

\smallskip
\noindent\textbf{Feedback Refinement Relation (FRR in short).} 
Let $\Qa\subseteq X\times \Xa$ be a relation s.t.\ $(x,\xa)\in\Qa$ iff $x\in\xa$. Then $\Qa$ is a \emph{feedback refinement relation (FRR)} from $\St$ to $\Sa$ 
written $\St\frr{\Qa}\Sa$ (see \cite{ReissigWeberRungger_2017_FRR}, Thm. III.5).
% 
That is, $\Qa$ is a strict relation,
i.e., for each $x$, there is some $\xa$ such that $(x,\xa)\in \Qa$, and 
for all $(x,\xa)\in \Qa$, we have
\begin{inparaenum}[(i)]
 \item $U_{\Sa}(\xa)\subseteq U_{\St}(x)$, and 
 \item $u\in U_{\Sa}(\xa) \Rightarrow \Qa(\Ft(x,u))\subseteq \Fa(\xa,u)$,
\end{inparaenum}
 where $U_{\St}(x):=\SetComp{u\in U}{\Ft(x,u)\neq \emptyset}$ and $U_{\Sa}(x):=\SetComp{u\in U}{\Fa(x,u)\neq \emptyset}$.
 
 \KM{The following part is new.}\\
 \smallskip
 \noindent\textbf{Controller and Closed Loop.}
 An abstract state-feedback controller of the abstract system $\Sa(\Sigma,\tau,\eta)$ is a function $\Ca:\Ya\rightarrow \Ua$. \footnote{This type of controller is called a static controller, as opposed to dynamic controller having internal dynamics. It can be shown that static controller is the optimal controller for a large class of control specifications---including Parity---the one which is the subject of this paper.}
The abstract closed loop is defined as the system $\Sacl(\Sigma,\tau,\eta,\Ca)=(\Xa,\Facl)$, where $\Facl:\Xa\rightarrow 2^{\Xa}$ is the one-step transition function defined as $\Facl:\xa\mapsto \Fa(\xa,\Ca(\xa))$.
 
 %The domain of the abstract controller is the domain of the function $\Ca$, written as $\dom{\Ca}$.
 The abstract controller $\Ca$ together with the FRR $\Qa$ induces a continuous controller for the sampled time system $\St(\Sigma,\tau)$ defined as $C:X\rightarrow \Ua$, $C:x \mapsto \Ca(\Qa(x)) $.
 This process of obtaining the continuous controller $C$ from the abstract controller $\Ca$ is called controller refinement, and $C$ is called the refined controller.
 The continuous closed loop for the sampled time system is defined as the system $\Stcl(\Sigma,\tau,C) = (X,\Ftcl)$, where $\Ftcl:X\rightarrow 2^X$ is the one-step transition function defined as $\Ftcl:x\mapsto \Ft(x,C(x))$.
 When obvious from the context, we drop the arguments and write $\Sacl$ and $\Stcl$ to denote the abstract closed loop and the continuous closed loop system respectively.
 
 \smallskip
 \noindent\textbf{Closed Loop Behaviors.}
 The closed loop behaviors of $\Sacl$ and $\Stcl$ are respectively defined as the sets of infinite trajectories:
 \begin{align*}
 	\Behacl(\Sacl) := \set{\xia\in \Xa^\infty \mid \forall k>0\;.\;\xia(k)\in \Facl(\xia(k-1))},\\
 	\Behtcl(\Stcl) := \set{\xi\in X^\infty \mid \forall k>0\;.\;\xi(k)\in \Ftcl(\xi(k-1))}.
 \end{align*}
 
 \smallskip
 \noindent\textbf{Abstraction Based Controller Synthesis.}
 We consider Parity specification as the control objective in this paper.
 A parity specification is defined using a tuple $\Spec=(L,\LF)$ s.t.\ $L$ is a finite set of colors $L\subset \mathbb{N}_0$, and $\LF:X\rightarrow L$ is a state labeling function.
 Given $\Spec$, and a trajectory $\xi\in \Behtcl(\Stcl)$, we define the operator $\inf:\Behtcl\rightarrow 2^L$ s.t.\ $\inf(\xi):=\set{ l\in L\mid \forall k_0\geq 0\;.\; \exists k_1>k_0\;.\; \LF(\xi(k_1)) = l}$.
 For a given $\Spec$, the continuous closed loop behavior $\xi \in \Behtcl(\Stcl)$ is said to satisfy the (even) parity specification if the largest color appearing in $\inf(\xi)$ is an even number, i.e.\ $\max{\inf(\xi)}$ is even.
 
 Throughout, we assume that the abstract state space $\Xa$ is consistent w.r.t.\ $\LF$, i.e.\ $\forall \xa\in \Xa\;.\;\forall (x_1,x_2)\in \xa\times \xa\;.\;\LF(x_1)=\LF(x_2)$.
 Then we can lift the parity specification $\Spec=(L,\LF)$ defined on $\St$ to a parity specification $\Speca=(L,\LFa)$ defined on $\Sa$ as follows: 
 $\LFa:\Xa\rightarrow L$ is the abstract color labeling function s.t.\ for any $\xa\in\Xa$,  $\LFa(\xa)=l$ if $\forall x\in \xa\;.\;\LF(x)=l$.
 Similar to the case of $\Behtcl$, we define the operator $\inf$ on $\Behacl$: $\inf(\xia):=\set{ l\in L \mid  \forall k_0\geq 0\;.\; \exists k_1>k_0\;.\; \LF(\xia(k_1)) = l}$.
 The satisfaction of the specification $\Speca$ by a given abstract closed trajectory in $\Behacl$ is defined as before:  An abstract trajectory $\xia\in \Behacl(\Sacl)$ is said to satisfy the even parity condition if $\max{\inf(\xia)}$ is even.
 
 We know from \cite[Thm.~VI.3]{reissig2016feedback} that if we can find an abstract controller $\Ca$, s.t.\ the abstract closed loop $\Sacl$ satisfies $\Speca$, then the refined controller $C$ induces a continuous closed loop $\Stcl$ that satisfies the specification $\Spec$.
 \KM{This result is for LTL specification. I am not sure what are the consequences when the specification is omega-regular.}

%\smallskip
%\noindent\textbf{Multi-Layered Controller and Closed Loop.}
%Given a multi-layered abstract system $\Saset$, a multi-layered controller is defined as  $\Cset=\set{\Ci{l}}_{\layer}$ where for all $l\in [1;L]$ $\Ci{l}=(\Uci{l}, \Ua,\Gci{l})$, $\Uci{l}$ is the controller domain, and $\Gci{l}:\Uci{l}\fun 2^{\Ua}$ is the feedback control map. $\Cset$ is composable with $\Saset$ if $\Ci{l}$ is composable with $\Sa{ l}$ for all $\layer$, i.e., $\Uci{l}\subseteq \Xa{ l}$. We denote by $\dom{\Cset}=\bigcup_\layer\Uci{l}$ the domain of $\Cset$.
%% 
%Given a multi-layered controller $\Cset$, we define the \emph{quantizer induced by $\Cset$} as the 
%map $\Qset:X\fun 2^{\Xaall}\setminus\set{\emptyset}$ with $\Xaall=\bigcup_\layer \Xa{l}$ s.t.\ for all $x\in X$ it holds that $\xa\in\Qset(x)$ iff %either
%% \begin{compactenum}[(i)]
%%  \item 
% there exists $\layer$ s.t.\
%  $\xa\in\Qa{l}(x)\cap\Uci{l}$
% and there exists no $l'>l$ and 
%  $\xa'\in\Qa{l'}(x)\cap\Uci{l'}$.
%% or 
%%  \item $\xa\in\Qa{1}(x)$ and $\not\exists\layer$ s.t.\ 
%% 	$\xa\in\Qa{l}(x)\cap\Uci{l}$.
%% \end{compactenum}
%%  
%We define $\img{\Qset}=\SetComp{\xa\in\Xaall}{\ExQ{x\in X}{\xa\in\Qset(x)}}$. 
%Intuitively, $\Qset$ maps states $x\in X$ to the \emph{coarsest} abstract state $\xa$ that is both related to $x$ and in the domain of $\Cset$.
%% (condition (i)). If such an abstract state does not exist, $\Qset$ maps $x$ to its related layer $l=1$ states (condition (ii)). 
%% $\Qa{l}$ is non-deterministic for states which lie at the boundary of two cells $\xa,\xa'\in\Xa{l}$. If such an $x$ happens to be located at the boundary of controller domains computed for different layers, $\Qset_z$ maps $x$ to two abstract cells within different layers.
%
%The \emph{closed loop system} formed by interconnecting
%$\Saset$ and $\Cset$ in \emph{feedback} 
%is defined by the system 
%$\Saset^{cl}=(\Xaall, \Faall^{cl})$ with $\Faall^{cl}:\img{\Qset}\fun2^{\img{\Qset}}$ s.t.\ 
%$\xa'\in\Faall^{cl}(\xa)$ iff there exists $\layer$, $\ua\in\Gci{l}(\xa)$ and $\xa''\in\Fa{ l}(\xa,\ua)$ s.t.\ $\xa'\in\Qset(\Qai{ l}(\xa''))$.
%% 
%As $\St{l}\frr{\Qa{l}} \Sa{l}$ for all $l\in[1;L]$, $\Cset$
%can be refined into a controller composable with $\Stset$ using $\Qset$ (see \cite{HsuMajumdarMallikSchmuck_HSCC18}, Sec.~3.4). This results in the closed loop system
%$\Stset^{cl}=(X, \Ftall^{cl})$ with $\Ftall^{cl}:X\fun 2^X$ s.t.\
%$x'\in\Ftall^{cl}(x)$ iff there exists $\xa\in \Qset(x)$, $\layer$ and $\ua\in\Gci{l}(\xa)$ s.t.\ $x'\in\Ft{ l}(x,\ua)$.
%% 
%The behavior of $\Stset^{cl}$ and $\Saset^{cl}$ are defined by 
%% \begin{subequations}\label{equ:def:Behcl}
% \begin{align*}
% \Behtclset&:=\textstyle\SetComp{\xi\in X^\infty}{\AllQ{1\leq k < |\xi|}{\xi(k)\in \Ftall^{cl}(\xi(k-1))}}\\%\label{equ:def:Behtcl}}\\
% \Behaclset&:=\textstyle\SetComp{\xia\in \Xaall^\infty}{\AllQ{1\leq k < {|\xia|}}{\xia(k)\in \Faall^{cl}(\xia(k-1))}}.%\label{equ:def:Behacl}
%\end{align*}
%% \end{subequations}
% Note that $\Behaclset$ contains trajectories composed from abstract states of different coarseness and $\Behtclset$ contains trajectories with non-uniform sampling time.
%
%
