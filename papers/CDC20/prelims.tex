\section{Preliminaries on Control Systems}

\smallskip
\noindent\textbf{Notation.}
We introduce an ordinal notation in the style of von Neumann.
The nonnegative integers are inductively defined as $0 = \emptyset$ and $n+1 = n \cup \{ n \}$.
Consequently, the first limit ordinal is the set of non-negative integers $\omega = \{ 0, 1, \ldots \}$ and the next two ordinals are $\omega+1$ and $\omega+2$.
It is easy to see that these ordinals are ordered by set inclusion. 
For any given set $S$, we use the notation $S^\omega$ to represent the set of all infinite sequences that can be formed by using the elements of the set $S$.

\smallskip
\noindent\textbf{Sampled-time Control System.}
A \emph{sampled-time control system} is a tuple $\SysT = (\Xc,\Uc,\Wnor,\ft)$, which consists of a state space $\Xc= \mathbb{R}^n$, a non-empty input space $\Uc\subseteq \mathbb{R}^m$, 
a bounded disturbance set $\Wnor\subset \mathbb{R}^n$,
and a transition function $\ft:\Xc\times \Uc\times\mathbb{R}^n \rightarrow \Xc$.

\smallskip
\noindent\textbf{Control Specifications.}
Let $\mathit{Win}\subseteq \Xc^\omega$ be a given control specification.
We consider two different control specifications: safety and parity (the latter being the canonical representation of temporal logic specifications).
For safety the set $\mathit{Win}$ is defined by $\mathit{Safety}(W) = \{ x_0 x_1 \ldots \in \Xc^\omega \mid x_i \notin W \text{ for all } i \in \omega \}$ for a given set $W \subseteq \Xc$ of unsafe states and requires that the systems state be always outside the set $W$.
For parity conditions the set $\mathit{Win}$ is defined by $\mathit{Parity}(\Phi) \coloneqq \{ x_0 x_1 \ldots \in \Xc^\omega \mid \limsup\set{\Phi(x_0), \Phi(x_1), \ldots} \text{ is even} \}$ for a function $\Phi \colon \Xc \to \omega$ assigning a priority to states, and requires that the maximum priority seen by the system infinitely often is even.
As per convention, we will use the term ``\emph{color}'' in place of ``priority'' while using parity conditions.
For notational convenience, we use $\Phi$ and $W$ in place of $\mathit{Parity}(\Phi)$ and $\mathit{Safety}(W)$, respectively.

Both parity and safety conditions permit memoryless (or positional) control strategies, which only depend on the current state.
This allows us to restrict ourselves to static state feedback controllers, introduced next.

\smallskip
\noindent\textbf{Controller and Closed-loop.}
A state-feedback controller, or simply a controller, for the control system $\SysT=(\Xc,\Uc,\Wnor,\ft)$ is a partial function $C:\Xc\rightarrow \Uc$.
We denote the closed loop formed by connecting $C$ to $\SysT$ in feedback as $\SysT\parallel C = (\dom{C},\mathbb{R}^n,\ftc)$, where $\dom{C} \subseteq \Xc$ is the domain of the controller $C$, the transition function $\ftc:\dom{C} \times \mathbb{R}^n \rightarrow\Xc$ is obtained from $\ft$ by using the rule $\ftc:(\xc,w)\mapsto \ft(\xc,C(\xc),w)$.

A closed-loop \emph{trajectory} of $\SysT\parallel C$ starting at a state $\xc_0\in \dom{C}$, exposed to disturbances from the set $W\subseteq \mathbb{R}^n$, is an infinite sequence $\traj^W(\xc_0)= (\xc_0,w_0)(\xc_1,w_1)\ldots$ s.t.\ for all $i\in \omega$, $w_i\in W$, and $\xc_{i+1}=\ftc(\xc_i,w_i)$.
The trajectory $\traj^W(\xc_0)$ is said to satisfy a given specification $\mathit{Win}$, denoted by $\traj^W(\xc_0)\vDash\mathit{Win}$, if the sequence $\xc_0\xc_1\ldots$ satisfies $\mathit{Win}$. Otherwise $\traj^W(\xc_0)$ violates $\mathit{Win}$, denoted by $\traj^W(\xc_0)\nvDash\mathit{Win}$. 

A controller $C$ is called \emph{sound} if for all $\xc\in \dom{C}$, and for every trajectory $\traj^{\Wnor}(\xc)$ (for every possible disturbance sequence from $\Wnor$), $\traj^{\Wnor}(\xc)\vDash\mathit{Win}$.
A sound controller $C$ is called \emph{maximal} if there is no other sound controller $C'$ s.t.\ $\dom{C'}\supset \dom{C}$.
We write $\Cfamily^{\SysT,\mathit{Win}}$ for the set of all sound and maximal controllers for a system $\SysT$ and a specification $\mathit{Win}$.