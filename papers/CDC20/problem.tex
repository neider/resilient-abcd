\section{Problem Statement}\label{sec:problem}

We define a \emph{risk-aware controller synthesis problem} using the tuple $\Prob=(\SysT,\Whi,\Spec)$, where $\SysT=(\Xc,\Uc,\Wnor,\fc)$ is a sampled-time control system that is normally exposed to disturbances from $\Wnor$, the set $\Whi\supset \Wnor$ is a set of higher disturbance spikes that $\Sys$ is only occasionally exposed to, and $\Spec$ is a parity specification over the state space $\Xc$.

For a risk-aware controller synthesis problem, we focus on the trajectories $\traj^{\Whi}(\xc_0)$ with disturbance values from the set $\Whi$, rather than $\traj^{\Wnor}(\xc_0)$.
Note that in practice, we interpret disturbances from the set $\Whi\setminus \Wnor$ as rare events.
Given any trajectory $\traj^{\Whi}(\xc_0)=(\xc_0,w_0)(\xc_1,w_1)\ldots$, define the operator $\NSpikes: \traj^{\Whi}(\xc_0)\mapsto \card \set{i \in \omega \mid w_i\in \Whi\setminus \Wnor}$, where $\card{S}$ is the cardinality of a given set $S$, and it is understood that $\Wnor$ is clear from the context.
Intuitively, $\NSpikes(\traj^{\Whi}(\xc_0))$ returns the number of times a disturbance outside $\Wnor$ (but in $\Whi$) occurred in the trajectory $\traj^{\Whi}(\xc_0)$.
A closed-loop trajectory $\traj^{\Whi}(\xc_0)$ is called \emph{spike-free} if $\NSpikes(\traj^{\Whi}(\xc_0))=0$.

Let $C\in \Cfamily^{\SysT,\Spec}$ be a controller, and $\alpha \in \omega + 2$.
We say that $C$ is \emph{$\alpha$-resilient} from a state $\xc_0 \in \Xc$ of $\SysT$, if every closed-loop trajectory $\traj^{\Whi}(\xc_0)$ of $\SysT\parallel C$ that starts in $\xc_0$ and satisfies $\NSpikes(\traj^{\Whi}(\xc_0)) < \alpha$ also satisfies the specification $\Spec$.
This means that a $k$-resilient controller with $k \in \omega$ satisfies $\Spec$ even under at most $k-1$ high disturbance spikes, an $\omega$-resilient controller satisfies $\Spec$ even under any finite number of high disturbance spikes, and an $(\omega + 1)$-resilient controller satisfies $\Spec$ even under infinitely many high disturbance spikes.

Next, we define the \emph{resilience} of a state $\xc \in \Xc$ to be
\begin{multline*}
	r_{\Prob}(\xc) = \sup \{ \alpha \in \omega + 2 \mid \text{there exists an} \\
	\text{$\alpha$-resilient controller from $\xc$} \}.
\end{multline*}
Note that $r_{\Prob}(\xc) > 0$ if and only if $\xc \in \dom{C}$, because any controller is $1$-resilient from $\xc$ if and only if $\xc\in \dom{C}$. 
% 
We call a controller $C^*\in \Cfamily^{\SysT,\Spec}$ \emph{optimally resilient}, if it is $r_{\Prob}(\xc)$-resilient from every state $\xc \in \Xc$.

In this paper, we address the following  problem: 

\emph{Given a risk-aware controller synthesis problem $\Prob$, find a procedure that will automatically synthesize an \emph{approximately} optimally resilient controller $C$}.

%
Following the paradigm of ABCD, we address this problem in three steps. We
\begin{inparaenum}[(i)]
 \item reduce the \emph{risk-aware controller synthesis problem} $\Prob$ into an \emph{abstract risk-aware controller synthesis problem} $\ProbA$ (see Sec.~\ref{sec:compute_abs}),
  \item we synthesize an \emph{optimally resilient controller}  $\widehat{C}^*$ for the problem $\ProbA$ (see Sec.~\ref{sec:synth}), and
 \item we refine  $\widehat{C}^*$ into an approximately optimally resilient controller $C$ for $\Prob$ which approaches the optimal solution $C^*$ if the grid size for the state and input grid goes to zero.
\end{inparaenum}

It is unknown whether the optimally resilient controller $C^*$ for the problem $\mathcal{P}$ always exists.
However, we show that an optimally resilient controller for the abstract problem $\widehat{\mathcal{P}}$ exists. We generate the controller $C$ by refining the optimally resilient controller $\widehat{C}^*$ for $\widehat{\mathcal{P}}$.
