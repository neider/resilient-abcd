%---------- Proof ----------
\clearpage

\section{Proof}

\subsection{Preliminaries: Infinite Games with Disturbances and Explicit Inputs}

The abstract synthesis problem $\ProbA = (\Gamma, \widehat{\Phi})$ is essentially an infinite game with the arena being the risk-aware abstraction $\Gamma=(\Xa,\Ua,\fanor,\fadist)$ and the winning condition being $\widehat{\Phi}$.
Following Neider, Weinert, and Zimmermann~\cite{DBLP:journals/acta/NeiderWZ20}, we define the disturbance and risk updates as updates on partial mappings $r \colon \Xa \to \omega$, which are called \emph{rankings}.
Intuitively, a ranking assigns resilience to some of the abstract states.
We denote the domain of $r$ by $\mathrm{dom}(r)$ and the image of $r$ by $\mathrm{im}(r)$.  \\
 A play in $\Gamma$ is an infinite sequence $\rho = (v_{0}, b_{0}), (v_{1}, b_{1}), (v_{2}, b_{2}), ... \in (\Xa \times \{0,1\})^{\omega}$ such that $ b_{0} = 0$ and $\forall j>0  \ \exists \ua \in \Ua: b_{j} = 0$ implies $v_{j} \in \fanor(v_{j-1}, \ua)$ and $b_{j} = 1$ implies $v_{j} \in \fadist(v_{j-1}, \ua)$. Hence, the additional bits $b_{j}$ for $j>0$ denote whether a normal or disturbance edge has been taken to move from $v_{j-1}$ to $v_{j}$ on input $\ua$. We say $\rho$ starts in $v_{0}$. A play prefix $(v_{0}, b_{0}), (v_{1}, b_{1}), ... (v_{j}, b_{j})$  is defined similarly and ends in $v_{j}$. The number of disturbances in play $\rho = (v_{0}, b_{0}), (v_{1}, b_{1}), (v_{2}, b_{2}), ... $ is defined as $\NSpikes(\rho) = | \{j \in \omega \ |\ b_{j} = 1 \}|$.\\
A strategy or controller for player 0 is defined as $\widehat{C} : \Xa \mapsto \Ua$ \\
A play $\rho = (v_{0}, b_{0}), (v_{1}, b_{1}), (v_{2}, b_{2}), ...$ is consistent with strategy $\sigma$, if $v_{j+1} \in \fanor(v_{j}, \sigma(v_{j}))$ for every $j$ with $v_{j} \in \Xa$ and $b_{j+1}= 0$\\

$\alpha$-resilience is defined the same way. The notion of min is taken care of by the definition of alpha resilience. \\
$r_{\ProbA}(\xa)$ also remains exactly the same which encodes our notion of min.

\emph{Resilient Controllers:}
Let $\ProbA$ be an abstract synthesis problem with the abstract state space~$\Xa$ and let $\alpha \in \omega+2$. A strategy~$\widehat{C}$ for Player~$0$ in $\ProbA$ is \emph{$\alpha$-resilient from~$\xa \in \Xa$} if every play~$\rho$ that starts in $\xa$, that is consistent with $\sigma$, and with~$\NSpikes(\rho) < \alpha$, is winning for Player~$0$. Thus, a $k$-resilient strategy with $k \in \omega$ is winning even under at most $k-1$ disturbances, an $\omega$-resilient strategy is winning even under any finite number of disturbances, and an $(\omega+1)$-resilient strategy is winning even under infinitely many disturbances. 

\begin{remark}
\label{remark_resilienceprops}
Let $\xa$ be a vertex.
\begin{enumerate}
	\item\label{remark_resilienceprops_mono} Let $\alpha , \alpha' \in \omega+2$ with $\alpha > \alpha'$. If a strategy is $\alpha$-resilient from $\xa$, then it is also $\alpha'$-resilient from $\xa$.
	\item\label{remark_resilienceprops_zeroresil} Every strategy is $0$-resilient from $\xa$.
	\item\label{remark_resilienceprops_winning} A strategy is~$1$-resilient from~$\xa$ if and only if it is winning for Player~$0$ from~$\xa$. 
\end{enumerate}
\end{remark}
We define the resilience of a vertex~$\xa$ of $\ProbA$ as 
\[
r_{\ProbA}(\xa) = \sup \{ \alpha\in\omega+2 \mid \text{Player~$0$ has an $\alpha$-resilient }
\]
\[
\text{ strategy for $\ProbA$ from $\xa$ } \}.
\]
Note that the definition is not antagonistic, i.e., it is not defined via strategies of Player~$1$. Nevertheless, due to the remarks above, resilient strategies generalize winning strategies. 

\subsection{Computing Optimally Resilient Controllers}

\subsubsection{Characterizing Vertices of Finite Resilience}
\begin{lemma}
\label{lemma_winningregionsvsresilience}
Let $\ProbA$ be a game and $\xa$ a vertex of $\ProbA$.
\begin{enumerate}
\item\label{lemma_winningregionsvsresilience_zero} $r_{\ProbA}(\xa) > 0$ if and only if $\xa \in \winreg_0(\game)$.
\item\label{lemma_winningregionsvsresilience_one} If $\ProbA$ is determined, then $r_{\ProbA}(\xa) = 0$ if and only if $\xa \in \winreg_1(\ProbA)$.\\
\end{enumerate}
\end{lemma}

\begin{lemma}
\label{lemma_disturbanceupdateproperties}
The disturbance update~$r'$ of a sound ranking~$r$ is sound and refines~$r$. \\
\end{lemma}

\begin{lemma}
\label{lemma_riskupdateproperties}
The risk update~$r'$ of a sound ranking~$r$ is sound and refines $r$. \\
\end{lemma}

\begin{lemma}
\label{lemma_ranktermination}
If $v \in \dom(r^*)$, then $r_{j_v}(v) = r_j(v)$ for all $j \ge j_v$. \\
\end{lemma}

\begin{corollary}
\label{corollary_computationprops}
We have $\im(r^*) = \set{0, 1, \ldots, n}$ for some $n < \size{\Xa}$ and $r^* = r_{2\size{\Xa}}$. \\
\end{corollary}

The main result of this section shows that $r^*$ characterizes the resilience of vertices of finite resilience. \\

\new{
The disturbance update $r'$ induces an ordering on the inputs  from every state $\xa$ such that $\xa \in \Xa \setminus \mathrm{dom}(r') \textrm{ and } \exists \ua \in \Ua. \ \fadist(q,u) \cap \mathrm{dom}(r) \neq \phi$. We call such an input $\ua$ a \textbf{weed input}. \\
Weed inputs cause suboptimality. See section \ref{section:weedinputs} \\
Strategy Pruning Removes weed inputs resulting in an optimal solution for the algorithm. \\
We define an ordering function for every state $\xa \in \Xa$:
\[
\lambda_{\xa}:\ \Ua \mapsto \  \mathbb{N}
\]
Initially,
\[
\lambda_{\xa}(\ua) = 0   \ \ \ \ \xa \in \Xa, \ua \in \Ua
\]
After a disturbance update, we increment the ordering of all the weed inputs $\ua$ of a state $\xa$ i.e. 
\[
\lambda_{\xa}(\ua) = \lambda_{\xa}(\ua) + 1
\]
}
\begin{lemma}
\label{lemma_rank_correctness}
Let $r^*$ be defined for $\ProbA$ as above, and let $\xa \in \Xa$.
\begin{enumerate}
	\item\label{lemma_rank_correctness_finite} If $\xa \in \dom(r^*)$, then $r_{\ProbA}(\xa) = r^*(\xa)$ \new{if and only if the risk update ensures that for all states $\xa \in \Xa$ and all inputs $\ua \in \Ua$, $max(\lambda_{\xa}(\ua))$) is considered. }
	\item\label{lemma_rank_correctness_infinite} If $\xa \notin \dom(r^*)$, then $r_{\ProbA}(\xa) \in \set{\omega, \omega+1}$. \\
\end{enumerate} 
\end{lemma}

\begin{corollary}
\label{corollary_resiliencedomain}
We have $r_{\ProbA}(\Xa) \cap \omega = \set{0, 1, \ldots, n}$ for some $n < \size{\Xa}$.	\\
\end{corollary} 

\subsubsection{Characterizing Vertices of Resilience $\omega+1$}
Let $\Gamma' = (\Xa', \Ua, \fanordash, \fadistdash)$ be the risk aware abstraction after the end of algorithm \ref{alg:computing-finite-resilience}. We solve the classical controller synthesis problem for the abstraction $\Gamma'' = (\Xa', \Ua, \fanordash\cup \fadistdash)$ (i.e., the abstraction that always allows for large disturbances) as explained in section \ref{sec:rigged-game-computation}. Let us call this resulting problem a rigged abstract synthesis problem $\ProbAR = (\Gamma'', \widehat{\Phi})$. \\
\begin{lemma}
\label{lemma_riggedcorrectness}
Let $\xa$ be a state in the abstract synthesis problem~$\ProbA$. Then, $\xa \in \winreg_0(\ProbAR)$ if and only if $r_{\ProbA}(\xa) = \omega+1$. \\
\end{lemma}

\begin{corollary}
\label{corollary_rigged} Let $\ProbA$ and $\ProbAR$ be defined as above and $\xa$ a state of $\ProbA$.
\begin{enumerate}
	\item\label{corollary_rigged_positional}
 Assume Player~$0$ has a positional winning strategy for $\ProbAR$ from $\xa$. Then, Player~$0$ has an $(\omega+1)$-resilient  positional strategy for $\ProbA$ from $\xa$. 
\item \label{corollary_rigged_finitestate}
Assume Player~$0$ has a finite-state winning strategy for $\ProbAR$ from $\xa$. Then, Player~$0$ has an $(\omega+1)$-resilient  finite-state strategy (of the same size) for $\ProbA$ from $\xa$. \\
\end{enumerate}
\end{corollary}


\subsubsection{Computing Optimally Resilient Controllers}


%
%Recall the requirements from Subsection~\ref{subsec_finiteresil} for a game $(\arena, \wincond)$: $\wincond$ is prefix-independent and the game~$\game_U$ is determined for every $U \subseteq V$, where we write $\game_U$ for the game~$(\arena, \wincond \cap \safety(U))$ for some $U \subseteq V$. 
% To prove the results of this subsection, we need to impose some additional effectiveness requirements: we require that each game~$\game_U$ and the rigged game~${\game_{\rig}}$ can be effectively solved. Also, we first assume that Player~$0$ has positional winning strategies for each of these games, which have to be effectively computable as well. We discuss the severity of these requirements in Section~\ref{sec_discussion}.

\begin{theorem}
\label{theorem_main}
%Let $\game$ satisfy all the above requirements. 

The resilience of $\ProbA$'s vertices and a positional optimally resilient controller $\widehat{C}^*$ can be effectively computed. \\
\end{theorem}

\begin{theorem}
\label{theorem_main_parity}
Optimally resilient strategies in parity games are positional and can be computed in quasipolynomial time.
\end{theorem}

\subsection{Informal Notes: Stanly}

%Here a play is a strict alternation. See how to add this later, if needed. \\

%Without Strategy Pruning, the algorithm is sound and refines the ranking function at every step by may not be optimal. \\

Lemma \ref{lemma_winningregionsvsresilience}, \ref{lemma_disturbanceupdateproperties}, \ref{lemma_riskupdateproperties}, \ref{lemma_ranktermination} hold and proof should follow from \cite{DBLP:journals/acta/NeiderWZ20}.

%Add an intermediate lemma to explain strategy pruning and then everything falls into place.

Lemma \ref{lemma_rank_correctness} breaks for algorithm \ref{alg:computing-finite-resilience} without Strategy Pruning. But with Strategy Pruning, Lemma \ref{lemma_rank_correctness} holds. When it breaks, it breaks at the disturbance update. Note that the disturbance update argument in the CSL paper does not depend on $\Ua$. In our case, it will depend on $\Ua$ because we don't always update a vertex with outgoing disturbance edge into $\mathrm{dom}(r)$. 

So our proof extension in this  would look something like this:
if $j_v$ is odd, then it was updated during a disturbance update. Hence there is some $v''$ and some $\ua$ such that $v'' \in \fadist(v',\ua)$ and there exists NO $\ua'$ that is of input type 3 by defintiion of disturbance update. 
But here it exists. Hence, done.