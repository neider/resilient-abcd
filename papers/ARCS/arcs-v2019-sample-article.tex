
\documentclass[a4paper,UKenglish,cleveref, autoref, thm-restate]{arcs-v2019}


%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory

\bibliographystyle{plainurl}% the mandatory bibstyle

\title{Resilient Abstraction-Based Controller Design} %TODO Please add

%\titlerunning{Dummy short title} %TODO optional, please use if title is longer than one line

\author{Stanly Samuel}{Indian Institute of Science, Bangalore, India}{}{}{}
%{https://orcid.org/0000-0002-1825-0097}{}
%{(Optional) author-specific funding acknowledgements}
\author{Kaushik Mallik}{Max Planck Institute for Software Systems, Kaiserslautern, Germany }{}{}{}
\author{Anne-Kathrin Schmuck}{Max Planck Institute for Software Systems, Kaiserslautern, Germany}{}{}{}
\author{Daniel Neider}{Max Planck Institute for Software Systems, Kaiserslautern, Germany}{}{}{}

%TODO mandatory, please use full name; only 1 author per \author macro; first two parameters are mandatory, other parameters can be empty. Please provide at least the name of the affiliation and the country. The full address is optional

%\author{Joan R. Public\footnote{Optional footnote, e.g. to mark corresponding author}}{Department of Informatics, Dummy College, [optional: Address], Country}{joanrpublic@dummycollege.org}{}{}
%{[orcid]}{[funding]}

\authorrunning{J.\,Q. Public and J.\,R. Public} 
%TODO mandatory. First: Use abbreviated first/middle names. Second (only in severe cases): Use first author plus 'et al.'

%\Copyright{John Q. Public and Joan R. Public} %TODO mandatory, please use full first names. LIPIcs license is "CC-BY";  http://creativecommons.org/licenses/by/3.0/

\input{macros}

% ----------- CCS Concepts -----------
\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10010147.10010178.10010213.10010214</concept_id>
<concept_desc>Computing methodologies~Computational control theory</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10010583.10010750.10010769</concept_id>
<concept_desc>Hardware~Safety critical systems</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Computing methodologies~Computational control theory}
\ccsdesc[500]{Hardware~Safety critical systems}

%\ccsdesc[100]{\textcolor{red}{Replace ccsdesc macro with valid one}} %TODO mandatory: Please choose ACM 2012 classifications from https://dl.acm.org/ccs/ccs_flat.cfm 

\keywords{Reactive Synthesis, Control Theory, Logic and Games} %TODO mandatory; please add comma-separated list of keywords



\nolinenumbers 

\hideLIPIcs  


\begin{document}

\maketitle

%TODO mandatory: add short abstract of the document
\begin{abstract}
 We consider the computation of resilient controllers for perturbed non-linear dynamical systems w.r.t. linear-time temporal logic specifications. We address this problem through the paradigm of Abstraction-Based Controller Design (ABCD) where a finite state abstraction of the perturbed system dynamics is constructed and utilized for controller synthesis. In this context, our contribution is twofold: (I) We construct abstractions which  model the impact of  occasional high disturbance spikes on the system via the so called disturbance edges. (II) We show that the application of resilient reactive synthesis techniques to these abstract models results in controllers which render the resulting closed loop system maximally resilient to these occasional high disturbance spikes. We have implemented this resilient ABCD workflow on top of SCOTS and showcase our method through multiple robot planning examples. \\
 This work has been accepted for full paper presentation at the 59th Conference on Decision and Control (CDC), 2020. This work was also accepted for poster presentation at the 23rd ACM International Conference on Hybrid Systems: Computation and Control (HSCC), 2020.
\end{abstract}

\section{Introduction}
\label{sec:intro}
 
With the advent of digital controllers being increasingly used to control safety-critical cyber-physical systems, there is a growing need to provide formal correctness guarantees of such controllers. A recent approach to achieve this goal is the so-called \emph{Abstraction-Based Controller Design} (ABCD) \cite{tabuada2009verification,belta2017formal}.
In ABCD, a continuous dynamical system and a temporal specification are provided as the input, and a controller is automatically generated as the output, with the guarantee that the closed-loop (obtained by connecting the controller with the continuous dynamical system in feedback) satisfies the given specification.
ABCD is usually performed in three-steps.  
First, a finite state abstraction is computed from the continuous dynamical system by discretizing the state and input space.
Second, the given specification is lifted to the state space of the abstraction, and a discrete control strategy is synthesized using techniques from reactive synthesis. 
Finally, the discrete control strategy is refined to a continuous controller for the given system, which serves as the output of the procedure.
ABCD has been implemented in various tools for a variety of classes of systems and specifications, with numerous improvements over the basic procedure \cite{cosyma,rungger2016scots,mascot,pFaces}.

As common in most ABCD approaches, we study non-linear continuous dynamical systems with unknown, but bounded external disturbances. These systems are typically modelled by a differential inclusion
\begin{equation}\label{equ:F}
 \dot{x}\in F(x,u)+W,
\end{equation}
where $x$ is the state of the system, $\dot{x}$ is the first order time derivative of the state, $u$ is the input chosen by the (to be designed) controller, and $W$ is a bounded set of external disturbances.
Due to the existence of disturbances, a fixed controller results in an uncertain closed loop trajectory. 
The larger the set $W$ is, the larger is the uncertainty, which makes the controller synthesis task harder.

For systems modeled by \eqref{equ:F}, a controller synthesized using ABCD is provably correct if the actual disturbances experienced by the system during operation are a subset of the design disturbance set $W$. % considered during abstraction computation. %(Eq.~\eqref{equ:def_f}).
For this reason, ABCD techniques typically assume the most pessimistic disturbance bound when defining the set $W$.
This could be unreasonably pessimistic whenever the disturbances seen most of the time are small, but occasional high disturbance \emph{spikes} may occur during operation. 
In this case, increasing $W$ to contain all possible disturbances may render the resulting abstract controller synthesis problem unrealizable, while a more optimistic choice of $W$ allows to design a controller but sacrifices rigorous correctness guarantees. That is, if a large disturbance spike occurs, the specification might be violated.

In our work, we address this trade-off between the increased pessimism and the reduced soundness guarantee of ABCD.
We automatically synthesize a controller which is correct w.r.t.\ a nominal disturbance $\Wnor$, and in addition, is \emph{risk-minimizing} w.r.t.\ larger disturbance spikes in $\Whi\supset \Wnor$ that may occur any time. 
Intuitively, this leads to controllers that aim at fulfilling the given temporal specification robustly.

Formally, we build on top of the basic three step procedure of ABCD. 
First, we obtain a special type of finite state abstraction for the given continuous dynamical system by adapting the framework of feedback-refinement relations \cite{ReissigWeberRungger_2017_FRR}; our abstraction has ``normal edges'' and ``disturbance edges'' to capture operation with disturbance $\Wnor$ and $\Whi\setminus \Wnor$, respectively.
Second, we compute a maximally-resilient control strategy for this abstraction by an adaptation of the two-player game algorithm to compute \emph{Optimally Resilient Strategies} as defined by 
%\citeauthor{DBLP:conf/csl/NeiderW018}~
\cite{dallal2016synthesis,DBLP:conf/csl/NeiderW018}. 
In particular, we first solve a game for the given specification over the graph induced by the normal edges of the abstraction.
This results in a control strategy for the abstract system which is correct-by-construction for the $\Wnor$ disturbance set. 
To take the high disturbance set $\Whi$ into account, we then compute the optimally resilient control strategy on top of the nominal solution. This is achieved by solving a series of games over the complete game graph by taking the disturbance edges into account.
This synthesis algorithm is modular in the sense that it can handle arbitrary $\omega$-regular winning conditions when provided with a solver for the disturbance-free game (under some additional mild assumption).
Finally, the control strategy so obtained can be refined to a continuous controller for the original system, while retaining the correctness guarantees and the resilience values.

This three step procedure is what we call the \emph{Resilient Abstraction-Based Controller Design}.

% !Tex root=extended-abstract.tex

\section{Experimental Results}
\label{sec:experiments}

We have implemented our \emph{resilient ABCD} approach in the tool RESCOT (REsilient SCOTS).
RESCOT is built on top of the existing ABCD tool called SCOTS \cite{rungger2016scots}, and is freely available\footnote{\url{https://bitbucket.org/stanlyjs/rescot/}}. We refer the reader to \cite{samuel2020resilient} for experimental results.
%%
%% Bibliography
%%

%% Please use bibtex, 

\bibliographystyle{plainurl}
\bibliography{bib}
\end{document}
